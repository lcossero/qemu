/*
 * QEMU System Emulator
 *
 * Copyright (c) 2003-2008 Fabrice Bellard
 * Copyright (c) 2009 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "qemu/osdep.h"
#include "tap_int.h"


#include <sys/ioctl.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <net/if.h>

#include "net/eth.h"
#include "net/net.h"
#include "clients.h"
#include "monitor/monitor.h"
#include "sysemu/sysemu.h"
#include "qapi/error.h"
#include "qemu/cutils.h"
#include "qemu/error-report.h"
#include "qemu/main-loop.h"
#include "qemu/sockets.h"

#include "net/tap.h"

#include "net/vhost_net.h"

#include "tantap.h"
#include <tansiv-client.h>
#include <netinet/ip.h> // struct ip, iphdr ...
#include <netinet/in.h> //enums IPPROTO ...
#include <netinet/udp.h> // struct udphdr...


// NOTE(msimonin) this comes from tap-linux.h
void tantap_set_sndbuf(int fd, const NetdevTanTapOptions *tap, Error **errp);
#define TAP_DEFAULT_SNDBUF 0
#define TUNSETSNDBUF   _IOW('T', 212, int)
// NOTE(msimonin) this comes from tap-linux.c
// but we use the NetdevTanTapOptions instead
void tantap_set_sndbuf(int fd, const NetdevTanTapOptions *tap, Error **errp)
{
    int sndbuf;

    sndbuf = !tap->has_sndbuf       ? TAP_DEFAULT_SNDBUF :
             tap->sndbuf > INT_MAX  ? INT_MAX :
             tap->sndbuf;

    if (!sndbuf) {
        sndbuf = INT_MAX;
    }

    if (ioctl(fd, TUNSETSNDBUF, &sndbuf) == -1 && tap->has_sndbuf) {
        error_setg_errno(errp, errno, "TUNSETSNDBUF ioctl failed");
    }
}



static void launch_script(const char *setup_script, const char *ifname,
                          int fd, Error **errp);

static void tantap_send(void *opaque);
static void tantap_writable(void *opaque);

static void tantap_update_fd_handler(TANTAPState *s)
{
    qemu_set_fd_handler(s->fd,
                        s->read_poll && s->enabled ? tantap_send : NULL,
                        s->write_poll && s->enabled ? tantap_writable : NULL,
                        s);
}

static void tantap_read_poll(TANTAPState *s, bool enable)
{
    s->read_poll = enable;
    tantap_update_fd_handler(s);
}

static void tantap_write_poll(TANTAPState *s, bool enable)
{
    s->write_poll = enable;
    tantap_update_fd_handler(s);
}

static void tantap_writable(void *opaque)
{
    TANTAPState *s = opaque;

    tantap_write_poll(s, false);

    qemu_flush_queued_packets(&s->nc);
}

static ssize_t tantap_write_packet(TANTAPState *s, const struct iovec *iov, int iovcnt)
{
    ssize_t len;

    do {
        /* printf("Print %d iovec to the tantap\n", iovcnt);
        for (int i=0; i<iovcnt; i++) {
            printf("iov_len=%ld\n", (iov + i)->iov_len);
        }*/
        len = writev(s->fd, iov, iovcnt);
    } while (len == -1 && errno == EINTR);

    if (len == -1 && errno == EAGAIN) {
        tantap_write_poll(s, true);
        return 0;
    }

    return len;
}

/* static ssize_t tantap_receive_iov(NetClientState *nc, const struct iovec *iov,
                               int iovcnt)
{
    printf("TANTAP] tantap_receive_iov\n");

    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    const struct iovec *iovp = iov;
    struct iovec iov_copy[iovcnt + 1];
    struct virtio_net_hdr_mrg_rxbuf hdr = { };

    if (s->host_vnet_hdr_len && !s->using_vnet_hdr) {
        iov_copy[0].iov_base = &hdr;
        iov_copy[0].iov_len =  s->host_vnet_hdr_len;
        memcpy(&iov_copy[1], iov, iovcnt * sizeof(*iov));
        iovp = iov_copy;
        iovcnt++;
    }

    return tantap_write_packet(s, iovp, iovcnt);
}
 */
static ssize_t tantap_receive_raw(NetClientState *nc, const uint8_t *buf, size_t size)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    struct iovec iov[2];
    int iovcnt = 0;
    struct virtio_net_hdr_mrg_rxbuf hdr = { };

    if (s->host_vnet_hdr_len) {
        iov[iovcnt].iov_base = &hdr;
        iov[iovcnt].iov_len  = s->host_vnet_hdr_len;
        iovcnt++;
    }

    iov[iovcnt].iov_base = (char *)buf;
    iov[iovcnt].iov_len  = size;
    iovcnt++;

    return tantap_write_packet(s, iov, iovcnt);
}


static void decode_iph(const uint8_t* buf){
    /*
    // Not decoding the whole header
    in_addr_t _src_addr = ntohl((((uint32_t)buf[26] << 24) +
                                ((uint32_t)buf[27] << 16) +
                                ((uint32_t)buf[28] <<  8) +
                                (uint32_t)buf[29]));
    */
    struct iphdr* iphdr = (struct iphdr*)(buf + ETH_HLEN);
    in_addr_t _src = iphdr->saddr;
    in_addr_t _dst = iphdr->daddr;
    char src_addr[INET_ADDRSTRLEN];
    char dst_addr[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(_src), src_addr, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &(_dst), dst_addr, INET_ADDRSTRLEN);
    printf("\t src=%s -> dst=%s \n", src_addr, dst_addr);
}



static void _tantap_decode_packet(const uint8_t *buf, size_t size, const char *prefix){
    // Let's decode what's inside the buf
    // Ref https://en.wikipedia.org/wiki/EtherType
    // This is the buf[12] and buf[13] are the two bytes of the ethertype field !
    printf("\n-- %s message decoding --\n", prefix);
    //dump_packet(buf, size);
    int proto = (((uint16_t)buf[12]) << 8) + buf[13];
    switch (proto) {
    case ETH_P_ARP:
        break;
    case ETH_P_IP:
    case ETH_P_IPV6:
        decode_iph(buf);
        break;
    case ETH_P_NCSI:
        break;
    default:
        break;
    }
}


static ssize_t tantap_receive(NetClientState *nc, const uint8_t *buf, size_t size)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    struct iovec iov[1];

    if (s->host_vnet_hdr_len && !s->using_vnet_hdr) {
        return tantap_receive_raw(nc, buf, size);
    }

    iov[0].iov_base = (char *)buf;
    iov[0].iov_len  = size;

    return tantap_write_packet(s, iov, 1);
}

/**
 * Handle udp packet
 *
 * In a first version we let go bootp packet (e.g dhcp) through the tap
 * Everything else will be sent to vsg
 *
 * Return semantic:
 *  >0: This packet has been send through vsg
 *  0 : This packet has been filtered out and will be handled by the underlying tap
 * code: vsg_send error code
 */
static int udp_input(TANTAPState *s, in_addr_t dest, const uint8_t *buf, size_t size) {
    const uint8_t *buf_cpy;
    if (s->host_vnet_hdr_len && !s->using_vnet_hdr)
        buf_cpy = buf;
    else
        buf_cpy = buf + s->host_vnet_hdr_len ;
    struct udphdr* udp_h = (struct udphdr*)(buf_cpy + ETH_HLEN + sizeof(struct iphdr));
    // NOTE(msimonin) Slirp udp_input is a bit more restrictive
    // if (ntohs(uh->uh_dport) == BOOTP_SERVER &&
    //     (ip->ip_dst.s_addr == slirp->vhost_addr.s_addr ||
    //      ip->ip_dst.s_addr == 0xffffffff)) {
    //     bootp_input(m);
    if (ntohs(udp_h->uh_dport) == BOOTP_SERVER) {
        // let it go through the tap
        return 0;
    }
    //dump_packet(buf, size);
    int code = vsg_send(s->nc.context, dest, size, buf);
    if (code == 0){
        return sizeof(buf);
    }
    return code;
}

/**
 * Handle tcp packet
 *
 * Return semantic:
 *  >=0: This packet has been send through vsg
 *  error code: fit to vsg_send error code
 */
static int tcp_input(TANTAPState *s, in_addr_t dest, const uint8_t *buf, size_t size) {
    int code = vsg_send(s->nc.context, dest, size, buf);
    if (code == 0){
        //dump_packet(buf, size);
        return sizeof(buf);
    }
    return code;
}

/**
 * Handle ip packet
 *
 * Return semantic:
 *  >0: This packet has been send through vsg
 *  0 : This packet has been filtered out and will be handled by the underlying tap
 *  -1: This packet is problematic / can't be send through vsg as expected
 */
static int ip_input(TANTAPState *s, const uint8_t *buf, size_t size) {

    const uint8_t *buf_cpy;
    if (s->host_vnet_hdr_len && !s->using_vnet_hdr)
        buf_cpy = buf;
    else
        buf_cpy = buf + s->host_vnet_hdr_len ;
    struct iphdr* ip_h = (struct iphdr*)(buf_cpy + ETH_HLEN);
    in_addr_t _src = ip_h->saddr;
    in_addr_t _dst = ip_h->daddr;
    char src_addr[INET_ADDRSTRLEN];
    char dst_addr[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(_src), src_addr, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &(_dst), dst_addr, INET_ADDRSTRLEN);
    //printf("TANTAP-ip-input] IP src=%s, dst=%s\n", src_addr, dst_addr);

    int proto = ip_h->protocol;
    int code = 0;
    switch (proto) {
    case IPPROTO_TCP:
        return tcp_input(s, _dst, buf, size);
    case IPPROTO_UDP:
        return udp_input(s, _dst, buf, size);
    case IPPROTO_ICMP:
        // let it go through vsg
        // _tantap_decode_packet(buf, size, "[G -> H]");
        code = vsg_send(s->nc.context, _dst, size, buf);
        if (code == 0){
            return sizeof(buf);
        } else {
            return code;
        }
    default:
        printf("TANTAP] IPPROTO un(known|suported) for now\n");
        break;
    }
    return 0;
}

/**
 * Handle ethernet frame that wants to reach the host network.
 * (Guest to Host communication)
 *
 * Return semantic:
 *  >0: This packet has been send through vsg
 *  =0: The packet needs to be sent to the tap (arp, dhcp...)
 *  code: vsg_send error
 */

static int if_input(TANTAPState *s, const uint8_t* buf, size_t size){
    // Let's decode what's coming from our guest (slirp style)
    // Ref https://en.wikipedia.org/wiki/EtherType
    // This is the buf[12] and buf[13] are the two bytes of the ethertype field !
    // Alternatively we could decode the whole ethernet headers
    const uint8_t *buf_cpy;
    if (s->host_vnet_hdr_len && !s->using_vnet_hdr)
        buf_cpy = buf;
    else
        buf_cpy = buf + s->host_vnet_hdr_len ;
    int ret = 0;
    int proto = (((uint16_t)buf_cpy[12]) << 8) + buf_cpy[13];
    switch (proto) {
    case ETH_P_ARP:
        break;
    case ETH_P_IP:
        ret = ip_input(s, buf, size);
        break;
    case ETH_P_IPV6:
        break;
    case ETH_P_NCSI:
        break;
    default:
        break;
    }
    return ret;
}

/**
 * Wrapper function that intercept packet before reaching the host network
 *
 * The packet is either intercepted and sent through vsg.
 * Interception decision relies on decoding the packet and decide according its
 * nature (tcp/udp...)
 * If it's not intercepted it will be send using the underlying tap.
 *
 */
static ssize_t tantap_complet_receive(NetClientState *nc, const uint8_t *buf, size_t size)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    int ret = if_input(s, buf, size);
    if (ret > 0)
        // This has been handle by vsg, we're good
        return ret;
    if (ret == 0)
        // This packet has been filtered out, and need to go through the tap (dhcp/arp...)
        return tantap_receive(nc, buf, size);
    if (ret == -1){
        // The impossible happens: we fail at sending to vsg this packet
        printf("TANTAP ERROR \n");
        _tantap_decode_packet(buf, size, "G->H");
        return 0;
    }
    return 0;
}

/*
 * This is called by tanqemu when there's some messages to process at a
 * deadline
 *
 */
static void tantap_vsg_receive_cb(uintptr_t arg)
{
    //printf("thread id = %ld receive the deadline callback\n", pthread_self());
    // NOTE(msimonin): When running in icount mode, the iothread is blocked when
    // the tanqemu deadline handler is called
    // look at cpu.c:qemu_tcg_rr_cpu_thread_fn

    // if (!qemu_mutex_iothread_locked()) {
    //     qemu_mutex_lock_iothread();
    // }
    NetClientState *nc = (NetClientState *) arg;
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    uint8_t *buf = s->buf;

    struct vsg_context* context = s->nc.context;
    // printf("Callback called for %s \n", s->nc.name);
    while (vsg_poll(context) == 0) {
        // overkill
        uint32_t msg_len = sizeof(s->buf);
        uint32_t src, dst;
        vsg_recv(context, &src, &dst, &msg_len, buf);

        // reinject the message to the nic
        // _tantap_decode_packet(buf, msg_len, "H -> G");

        // debug
        char src_addr[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &src, src_addr, INET_ADDRSTRLEN);
        // printf("Receive a message [src_decode=%s] -> transfering to NIC \n", src_addr);

        qemu_send_packet(nc, buf, msg_len);
        qemu_flush_queued_packets(nc->peer);
    }
    // if (qemu_mutex_iothread_locked()) {
    //     qemu_mutex_unlock_iothread();
    // }
}


static void tantap_send_completed(NetClientState *nc, ssize_t len)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    tantap_read_poll(s, true);
}

/* Should be unused for most of the traffic when using tansiv*/
static void tantap_send(void *opaque)
{
    TANTAPState *s = opaque;
    int size;
    int packets = 0;

    while (true) {
        uint8_t *buf = s->buf;
        uint8_t min_pkt[ETH_ZLEN];
        size_t min_pktsz = sizeof(min_pkt);

        size = tap_read_packet(s->fd, s->buf, sizeof(s->buf));
        if (size <= 0) {
            break;
        }
        
        if (s->host_vnet_hdr_len && !s->using_vnet_hdr) {
            buf  += s->host_vnet_hdr_len;
            size -= s->host_vnet_hdr_len;
        }

        if (net_peer_needs_padding(&s->nc)) {
            if (eth_pad_short_frame(min_pkt, &min_pktsz, buf, size)) {
                buf = min_pkt;
                size = min_pktsz;
            }
        }

        // _tantap_decode_packet(buf, size, "H -> G");
        size = qemu_send_packet_async(&s->nc, buf, size, tantap_send_completed);
        if (size == 0) {
            tantap_read_poll(s, false);
            break;
        } else if (size < 0) {
            break;
        }

        /*
         * When the host keeps receiving more packets while tantap_send() is
         * running we can hog the QEMU global mutex.  Limit the number of
         * packets that are processed per tantap_send() callback to prevent
         * stalling the guest.
         */
        packets++;
        if (packets >= 50) {
            break;
        }
    }
}

static bool tantap_has_ufo(NetClientState *nc)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);

    assert(nc->info->type == NET_CLIENT_DRIVER_TANTAP);

    return s->has_ufo;
}

static bool tantap_has_vnet_hdr(NetClientState *nc)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);

    assert(nc->info->type == NET_CLIENT_DRIVER_TANTAP);

    return !!s->host_vnet_hdr_len;
}

static bool tantap_has_vnet_hdr_len(NetClientState *nc, int len)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);

    assert(nc->info->type == NET_CLIENT_DRIVER_TANTAP);

    return !!tap_probe_vnet_hdr_len(s->fd, len);
}

static void tantap_set_vnet_hdr_len(NetClientState *nc, int len)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);

    assert(nc->info->type == NET_CLIENT_DRIVER_TANTAP);
    assert(len == sizeof(struct virtio_net_hdr_mrg_rxbuf) ||
           len == sizeof(struct virtio_net_hdr) ||
           len == sizeof(struct virtio_net_hdr_v1_hash));

    tap_fd_set_vnet_hdr_len(s->fd, len);
    s->host_vnet_hdr_len = len;
}

static void tantap_using_vnet_hdr(NetClientState *nc, bool using_vnet_hdr)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);

    assert(nc->info->type == NET_CLIENT_DRIVER_TANTAP);
    assert(!!s->host_vnet_hdr_len == using_vnet_hdr);

    s->using_vnet_hdr = using_vnet_hdr;
}

static int tantap_set_vnet_le(NetClientState *nc, bool is_le)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);

    return tap_fd_set_vnet_le(s->fd, is_le);
}

static int tantap_set_vnet_be(NetClientState *nc, bool is_be)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);

    return tap_fd_set_vnet_be(s->fd, is_be);
}

static void tantap_set_offload(NetClientState *nc, int csum, int tso4,
                     int tso6, int ecn, int ufo)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    if (s->fd < 0) {
        return;
    }

    tap_fd_set_offload(s->fd, csum, tso4, tso6, ecn, ufo);
}

static void tantap_exit_notify(Notifier *notifier, void *data)
{
    TANTAPState *s = container_of(notifier, TANTAPState, exit);
    Error *err = NULL;

    if (s->down_script[0]) {
        launch_script(s->down_script, s->down_script_arg, s->fd, &err);
        if (err) {
            error_report_err(err);
        }
    }
}

static void tantap_cleanup(NetClientState *nc)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);

    if (s->vhost_net) {
        vhost_net_cleanup(s->vhost_net);
        g_free(s->vhost_net);
        s->vhost_net = NULL;
    }

    qemu_purge_queued_packets(nc);

    tantap_exit_notify(&s->exit, NULL);
    qemu_remove_exit_notifier(&s->exit);

    tantap_read_poll(s, false);
    tantap_write_poll(s, false);
    close(s->fd);
    s->fd = -1;
}

static void tantap_poll(NetClientState *nc, bool enable)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    tantap_read_poll(s, enable);
    tantap_write_poll(s, enable);
}

static bool tantap_set_steering_ebpf(NetClientState *nc, int prog_fd)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    assert(nc->info->type == NET_CLIENT_DRIVER_TANTAP);

    return tap_fd_set_steering_ebpf(s->fd, prog_fd) == 0;
}

int tantap_get_fd(NetClientState *nc)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    assert(nc->info->type == NET_CLIENT_DRIVER_TANTAP);
    return s->fd;
}

static NetClientInfo net_tantap_info = {
    .type = NET_CLIENT_DRIVER_TANTAP,
    .size = sizeof(TANTAPState),
    .receive = tantap_complet_receive,
    // Adding our own callbacks
    //.receive = tantap_receive,
    //.receive_raw = tantap_receive_raw,
    //.receive_iov = tantap_receive_iov,
    .poll = tantap_poll,
    .cleanup = tantap_cleanup,
    .has_ufo = tantap_has_ufo,
    .has_vnet_hdr = tantap_has_vnet_hdr,
    .has_vnet_hdr_len = tantap_has_vnet_hdr_len,
    .using_vnet_hdr = tantap_using_vnet_hdr,
    .set_offload = tantap_set_offload,
    .set_vnet_hdr_len = tantap_set_vnet_hdr_len,
    .set_vnet_le = tantap_set_vnet_le,
    .set_vnet_be = tantap_set_vnet_be,
    .set_steering_ebpf = tantap_set_steering_ebpf,
    .vsg_receive_cb = tantap_vsg_receive_cb
};

static TANTAPState *net_tantap_fd_init(NetClientState *peer,
                                 const char *model,
                                 const char *name,
                                 int fd,
                                 int vnet_hdr)
{
    NetClientState *nc;
    TANTAPState *s;

    nc = qemu_new_net_client(&net_tantap_info, peer, model, name);

    s = DO_UPCAST(TANTAPState, nc, nc);

    s->fd = fd;
    s->host_vnet_hdr_len = vnet_hdr ? sizeof(struct virtio_net_hdr) : 0;
    s->using_vnet_hdr = false;
    s->has_ufo = tap_probe_has_ufo(s->fd);
    s->enabled = true;
    tantap_set_offload(&s->nc, 0, 0, 0, 0, 0);
    /*
     * Make sure host header length is set correctly in tantap:
     * it might have been modified by another instance of qemu.
     */
    if (tap_probe_vnet_hdr_len(s->fd, s->host_vnet_hdr_len)) {
        tap_fd_set_vnet_hdr_len(s->fd, s->host_vnet_hdr_len);
    }
    tantap_read_poll(s, true);
    s->vhost_net = NULL;

    s->exit.notify = tantap_exit_notify;
    qemu_add_exit_notifier(&s->exit);

    return s;
}

static void launch_script(const char *setup_script, const char *ifname,
                          int fd, Error **errp)
{
    int pid, status;
    char *args[3];
    char **parg;

    /* try to launch network script */
    pid = fork();
    if (pid < 0) {
        error_setg_errno(errp, errno, "could not launch network script %s",
                         setup_script);
        return;
    }
    if (pid == 0) {
        int open_max = sysconf(_SC_OPEN_MAX), i;

        for (i = 3; i < open_max; i++) {
            if (i != fd) {
                close(i);
            }
        }
        parg = args;
        *parg++ = (char *)setup_script;
        *parg++ = (char *)ifname;
        *parg = NULL;
        execv(setup_script, args);
        _exit(1);
    } else {
        while (waitpid(pid, &status, 0) != pid) {
            /* loop */
        }

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            return;
        }
        error_setg(errp, "network script %s failed with status %d",
                   setup_script, status);
    }
}

static int recv_fd(int c)
{
    int fd;
    uint8_t msgbuf[CMSG_SPACE(sizeof(fd))];
    struct msghdr msg = {
        .msg_control = msgbuf,
        .msg_controllen = sizeof(msgbuf),
    };
    struct cmsghdr *cmsg;
    struct iovec iov;
    uint8_t req[1];
    ssize_t len;

    cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(fd));
    msg.msg_controllen = cmsg->cmsg_len;

    iov.iov_base = req;
    iov.iov_len = sizeof(req);

    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    len = recvmsg(c, &msg, 0);
    if (len > 0) {
        memcpy(&fd, CMSG_DATA(cmsg), sizeof(fd));
        return fd;
    }

    return len;
}

static int net_bridge_run_helper(const char *helper, const char *bridge,
                                 Error **errp)
{
    sigset_t oldmask, mask;
    g_autofree char *default_helper = NULL;
    int pid, status;
    char *args[5];
    char **parg;
    int sv[2];

    sigemptyset(&mask);
    sigaddset(&mask, SIGCHLD);
    sigprocmask(SIG_BLOCK, &mask, &oldmask);

    if (!helper) {
        helper = default_helper = get_relocated_path(DEFAULT_BRIDGE_HELPER);
    }

    if (socketpair(PF_UNIX, SOCK_STREAM, 0, sv) == -1) {
        error_setg_errno(errp, errno, "socketpair() failed");
        return -1;
    }

    /* try to launch bridge helper */
    pid = fork();
    if (pid < 0) {
        error_setg_errno(errp, errno, "Can't fork bridge helper");
        return -1;
    }
    if (pid == 0) {
        int open_max = sysconf(_SC_OPEN_MAX), i;
        char *fd_buf = NULL;
        char *br_buf = NULL;
        char *helper_cmd = NULL;

        for (i = 3; i < open_max; i++) {
            if (i != sv[1]) {
                close(i);
            }
        }

        fd_buf = g_strdup_printf("%s%d", "--fd=", sv[1]);

        if (strrchr(helper, ' ') || strrchr(helper, '\t')) {
            /* assume helper is a command */

            if (strstr(helper, "--br=") == NULL) {
                br_buf = g_strdup_printf("%s%s", "--br=", bridge);
            }

            helper_cmd = g_strdup_printf("%s %s %s %s", helper,
                            "--use-vnet", fd_buf, br_buf ? br_buf : "");

            parg = args;
            *parg++ = (char *)"sh";
            *parg++ = (char *)"-c";
            *parg++ = helper_cmd;
            *parg++ = NULL;

            execv("/bin/sh", args);
            g_free(helper_cmd);
        } else {
            /* assume helper is just the executable path name */

            br_buf = g_strdup_printf("%s%s", "--br=", bridge);

            parg = args;
            *parg++ = (char *)helper;
            *parg++ = (char *)"--use-vnet";
            *parg++ = fd_buf;
            *parg++ = br_buf;
            *parg++ = NULL;

            execv(helper, args);
        }
        g_free(fd_buf);
        g_free(br_buf);
        _exit(1);

    } else {
        int fd;
        int saved_errno;

        close(sv[1]);

        do {
            fd = recv_fd(sv[0]);
        } while (fd == -1 && errno == EINTR);
        saved_errno = errno;

        close(sv[0]);

        while (waitpid(pid, &status, 0) != pid) {
            /* loop */
        }
        sigprocmask(SIG_SETMASK, &oldmask, NULL);
        if (fd < 0) {
            error_setg_errno(errp, saved_errno,
                             "failed to recv file descriptor");
            return -1;
        }
        if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
            error_setg(errp, "bridge helper failed");
            return -1;
        }
        return fd;
    }
}


static int net_tantap_init(const NetdevTanTapOptions *tantap, int *vnet_hdr,
                        const char *setup_script, char *ifname,
                        size_t ifname_sz, int mq_required, Error **errp)
{
    Error *err = NULL;
    int fd, vnet_hdr_required;

    if (tantap->has_vnet_hdr) {
        *vnet_hdr = tantap->vnet_hdr;
        vnet_hdr_required = *vnet_hdr;
    } else {
        *vnet_hdr = 1;
        vnet_hdr_required = 0;
    }

    TFR(fd = tap_open(ifname, ifname_sz, vnet_hdr, vnet_hdr_required,
                      mq_required, errp));
    if (fd < 0) {
        return -1;
    }

    if (setup_script &&
        setup_script[0] != '\0' &&
        strcmp(setup_script, "no") != 0) {
        launch_script(setup_script, ifname, fd, &err);
        if (err) {
            error_propagate(errp, err);
            close(fd);
            return -1;
        }
    }

    return fd;
}

#define MAX_TANTAP_QUEUES 1024


TANTAPState *tantap_state;
static void net_init_tantap_one(const NetdevTanTapOptions *tantap, NetClientState *peer,
                             const char *model, const char *name,
                             const char *ifname, const char *script,
                             const char *downscript, const char *vhostfdname,
                             int vnet_hdr, int fd, Error **errp)
{
    Error *err = NULL;
    TANTAPState *s = net_tantap_fd_init(peer, model, name, fd, vnet_hdr);
    tantap_state = s;
    int vhostfd;
    // NOTE(msimonin) cross your fingers.. casting to NetdevTapOptions
    // It seems that tap_set_sndbuf is just reading stuffs from the Options
    // so
    // 1. build a temporary NetdevTapOptions by copying the fields and pass it
    // 2. extract/copy this function and use it here
    // 2. or change the definition of the NetdevTanTapOptions to be
    // struct NetdevTanTapOptions {
    //   struct NetdevTapOptions n;
    //   // our options here
    //}
    // this way we could just pass &tantap->nc to the function without risking
    // the end of the universe
    //
    // tap_set_sndbuf(s->fd, (NetdevTapOptions *)tantap, &err);
    //
    // I chose 2. for now
    tantap_set_sndbuf(s->fd, tantap, &err);
    if (err) {
        error_propagate(errp, err);
        return;
    }

    if (tantap->has_fd || tantap->has_fds) {
        qemu_set_info_str(&s->nc, "fd=%d", fd);
    } else if (tantap->has_helper) {
        qemu_set_info_str(&s->nc, "helper=%s", tantap->helper);
    } else {
        qemu_set_info_str(&s->nc, "ifname=%s,script=%s,downscript=%s", ifname,
                          script, downscript);

        if (strcmp(downscript, "no") != 0) {
            snprintf(s->down_script, sizeof(s->down_script), "%s", downscript);
            snprintf(s->down_script_arg, sizeof(s->down_script_arg),
                     "%s", ifname);
        }
    }

    if (tantap->has_vhost ? tantap->vhost :
        vhostfdname || (tantap->has_vhostforce && tantap->vhostforce)) {
        VhostNetOptions options;

        options.backend_type = VHOST_BACKEND_TYPE_KERNEL;
        options.net_backend = &s->nc;
        if (tantap->has_poll_us) {
            options.busyloop_timeout = tantap->poll_us;
        } else {
            options.busyloop_timeout = 0;
        }

        if (vhostfdname) {
            vhostfd = monitor_fd_param(monitor_cur(), vhostfdname, &err);
            if (vhostfd == -1) {
                if (tantap->has_vhostforce && tantap->vhostforce) {
                    error_propagate(errp, err);
                } else {
                    warn_report_err(err);
                }
                goto failed;
            }
            if (!g_unix_set_fd_nonblocking(vhostfd, true, NULL)) {
                error_setg_errno(errp, errno, "%s: Can't use file descriptor %d",
                                 name, fd);
                goto failed;
            }
        } else {
            vhostfd = open("/dev/vhost-net", O_RDWR);
            if (vhostfd < 0) {
                if (tantap->has_vhostforce && tantap->vhostforce) {
                    error_setg_errno(errp, errno,
                                     "tantap: open vhost char device failed");
                } else {
                    warn_report("tantap: open vhost char device failed: %s",
                                strerror(errno));
                }
                goto failed;
            }
            if (!g_unix_set_fd_nonblocking(vhostfd, true, NULL)) {
                error_setg_errno(errp, errno, "Failed to set FD nonblocking");
                goto failed;
            }
        }
        options.opaque = (void *)(uintptr_t)vhostfd;
        options.nvqs = 2;

        s->vhost_net = vhost_net_init(&options);
        if (!s->vhost_net) {
            if (tantap->has_vhostforce && tantap->vhostforce) {
                error_setg(errp, VHOST_NET_INIT_FAILED);
            } else {
                warn_report(VHOST_NET_INIT_FAILED);
            }
            goto failed;
        }
    } else if (vhostfdname) {
        error_setg(errp, "vhostfd(s)= is not valid without vhost");
        goto failed;
    }

    return;

failed:
    qemu_del_net_client(&s->nc);
}

static int get_fds(char *str, char *fds[], int max)
{
    char *ptr = str, *this;
    size_t len = strlen(str);
    int i = 0;

    while (i < max && ptr < str + len) {
        this = strchr(ptr, ':');

        if (this == NULL) {
            fds[i] = g_strdup(ptr);
        } else {
            fds[i] = g_strndup(ptr, this - ptr);
        }

        i++;
        if (this == NULL) {
            break;
        } else {
            ptr = this + 1;
        }
    }

    /* NOTE(msimonin) vsg init sequence here */


    return i;
}



int net_init_tantap(const Netdev *netdev, const char *name,
                 NetClientState *peer, Error **errp)
{
    printf("Entering TANTAP backend\n");

    const NetdevTanTapOptions *tantap;
    int fd, vnet_hdr = 0, i = 0, queues;
    /* for the no-fd, no-helper case */
    const char *script = NULL;
    const char *downscript = NULL;
    Error *err = NULL;
    const char *vhostfdname;
    char ifname[128];
    int ret = 0;

    assert(netdev->type == NET_CLIENT_DRIVER_TANTAP);
    tantap = &netdev->u.tantap;
    queues = tantap->has_queues ? tantap->queues : 1;
    vhostfdname = tantap->has_vhostfd ? tantap->vhostfd : NULL;
    script = tantap->has_script ? tantap->script : NULL;
    downscript = tantap->has_downscript ? tantap->downscript : NULL;

    /* QEMU hubs do not support multiqueue tantap, in this case peer is set.
     * For -netdev, peer is always NULL. */
    if (peer && (tantap->has_queues || tantap->has_fds || tantap->has_vhostfds)) {
        error_setg(errp, "Multiqueue tantap cannot be used with hubs");
        return -1;
    }

    if (tantap->has_fd) {
        if (tantap->has_ifname || tantap->has_script || tantap->has_downscript ||
            tantap->has_vnet_hdr || tantap->has_helper || tantap->has_queues ||
            tantap->has_fds || tantap->has_vhostfds) {
            error_setg(errp, "ifname=, script=, downscript=, vnet_hdr=, "
                       "helper=, queues=, fds=, and vhostfds= "
                       "are invalid with fd=");
            return -1;
        }

        fd = monitor_fd_param(monitor_cur(), tantap->fd, errp);
        if (fd == -1) {
            return -1;
        }

        if (!g_unix_set_fd_nonblocking(fd, true, NULL)) {
            error_setg_errno(errp, errno, "%s: Can't use file descriptor %d",
                             name, fd);
            close(fd);
            return -1;
        }

        vnet_hdr = tap_probe_vnet_hdr(fd, errp);
        if (vnet_hdr < 0) {
            close(fd);
            return -1;
        }

        net_init_tantap_one(tantap, peer, "tantap", name, NULL,
                         script, downscript,
                         vhostfdname, vnet_hdr, fd, &err);
        if (err) {
            error_propagate(errp, err);
            close(fd);
            return -1;
        }
    } else if (tantap->has_fds) {
        char **fds;
        char **vhost_fds;
        int nfds = 0, nvhosts = 0;

        if (tantap->has_ifname || tantap->has_script || tantap->has_downscript ||
            tantap->has_vnet_hdr || tantap->has_helper || tantap->has_queues ||
            tantap->has_vhostfd) {
            error_setg(errp, "ifname=, script=, downscript=, vnet_hdr=, "
                       "helper=, queues=, and vhostfd= "
                       "are invalid with fds=");
            return -1;
        }

        fds = g_new0(char *, MAX_TANTAP_QUEUES);
        vhost_fds = g_new0(char *, MAX_TANTAP_QUEUES);

        nfds = get_fds(tantap->fds, fds, MAX_TANTAP_QUEUES);
        if (tantap->has_vhostfds) {
            nvhosts = get_fds(tantap->vhostfds, vhost_fds, MAX_TANTAP_QUEUES);
            if (nfds != nvhosts) {
                error_setg(errp, "The number of fds passed does not match "
                           "the number of vhostfds passed");
                ret = -1;
                goto free_fail;
            }
        }

        for (i = 0; i < nfds; i++) {
            fd = monitor_fd_param(monitor_cur(), fds[i], errp);
            if (fd == -1) {
                ret = -1;
                goto free_fail;
            }

            ret = g_unix_set_fd_nonblocking(fd, true, NULL);
            if (!ret) {
                error_setg_errno(errp, errno, "%s: Can't use file descriptor %d",
                                 name, fd);
                goto free_fail;
            }

            if (i == 0) {
                vnet_hdr = tap_probe_vnet_hdr(fd, errp);
                if (vnet_hdr < 0) {
                    ret = -1;
                    goto free_fail;
                }
            } else if (vnet_hdr != tap_probe_vnet_hdr(fd, NULL)) {
                error_setg(errp,
                           "vnet_hdr not consistent across given tantap fds");
                ret = -1;
                goto free_fail;
            }

            net_init_tantap_one(tantap, peer, "tantap", name, ifname,
                             script, downscript,
                             tantap->has_vhostfds ? vhost_fds[i] : NULL,
                             vnet_hdr, fd, &err);
            if (err) {
                error_propagate(errp, err);
                ret = -1;
                goto free_fail;
            }
        }

free_fail:
        for (i = 0; i < nvhosts; i++) {
            g_free(vhost_fds[i]);
        }
        for (i = 0; i < nfds; i++) {
            g_free(fds[i]);
        }
        g_free(fds);
        g_free(vhost_fds);
        return ret;
    } else if (tantap->has_helper) {
        if (tantap->has_ifname || tantap->has_script || tantap->has_downscript ||
            tantap->has_vnet_hdr || tantap->has_queues || tantap->has_vhostfds) {
            error_setg(errp, "ifname=, script=, downscript=, vnet_hdr=, "
                       "queues=, and vhostfds= are invalid with helper=");
            return -1;
        }

        fd = net_bridge_run_helper(tantap->helper,
                                   tantap->has_br ?
                                   tantap->br : DEFAULT_BRIDGE_INTERFACE,
                                   errp);
        if (fd == -1) {
            return -1;
        }

        if (!g_unix_set_fd_nonblocking(fd, true, NULL)) {
            error_setg_errno(errp, errno, "Failed to set FD nonblocking");
            return -1;
        }
        vnet_hdr = tap_probe_vnet_hdr(fd, errp);
        if (vnet_hdr < 0) {
            close(fd);
            return -1;
        }


        net_init_tantap_one(tantap, peer, "bridge", name, ifname,
                         script, downscript, vhostfdname,
                         vnet_hdr, fd, &err);
        if (err) {
            error_propagate(errp, err);
            close(fd);
            return -1;
        }
    } else {
        g_autofree char *default_script = NULL;
        g_autofree char *default_downscript = NULL;
        if (tantap->has_vhostfds) {
            error_setg(errp, "vhostfds= is invalid if fds= wasn't specified");
            return -1;
        }

        if (!script) {
            script = default_script = get_relocated_path(DEFAULT_NETWORK_SCRIPT);
        }
        if (!downscript) {
            downscript = default_downscript =
                                 get_relocated_path(DEFAULT_NETWORK_DOWN_SCRIPT);
        }

        if (tantap->has_ifname) {
            pstrcpy(ifname, sizeof ifname, tantap->ifname);
        } else {
            ifname[0] = '\0';
        }

        for (i = 0; i < queues; i++) {
            fd = net_tantap_init(tantap, &vnet_hdr, i >= 1 ? "no" : script,
                              ifname, sizeof ifname, queues > 1, errp);
            if (fd == -1) {
                return -1;
            }

            if (queues > 1 && i == 0 && !tantap->has_ifname) {
                if (tap_fd_get_ifname(fd, ifname)) {
                    error_setg(errp, "Fail to get ifname");
                    close(fd);
                    return -1;
                }
            }

            net_init_tantap_one(tantap, peer, "tantap", name, ifname,
                             i >= 1 ? "no" : script,
                             i >= 1 ? "no" : downscript,
                             vhostfdname, vnet_hdr, fd, &err);
            if (err) {
                error_propagate(errp, err);
                close(fd);
                return -1;
            }
        }
    }

    return 0;
}

VHostNetState *tantap_get_vhost_net(NetClientState *nc)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    assert(nc->info->type == NET_CLIENT_DRIVER_TANTAP);
    return s->vhost_net;
}

int tantap_enable(NetClientState *nc)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    int ret;

    if (s->enabled) {
        return 0;
    } else {
        ret = tap_fd_enable(s->fd);
        if (ret == 0) {
            s->enabled = true;
            tantap_update_fd_handler(s);
        }
        return ret;
    }
}

int tantap_disable(NetClientState *nc)
{
    TANTAPState *s = DO_UPCAST(TANTAPState, nc, nc);
    int ret;

    if (s->enabled == 0) {
        return 0;
    } else {
        ret = tap_fd_disable(s->fd);
        if (ret == 0) {
            qemu_purge_queued_packets(nc);
            s->enabled = false;
            tantap_update_fd_handler(s);
        }
        return ret;
    }
}
